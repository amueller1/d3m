Installation
------------

This package works with Python 3.6+. You need to have the following
packages installed on the system (for Debian/Ubuntu):

-  ``libssl-dev``
-  ``libcurl4-openssl-dev``

You can install latest stable version from `PyPI <https://pypi.org/>`__:

::

    $ pip3 install --process-dependency-links d3m

If you also want support for `Arrow <https://arrow.apache.org/>`__, use:

::

    $ pip3 install --process-dependency-links d3m[arrow]

To install latest development version:

::

    $ pip3 install --process-dependency-links git+https://gitlab.com/datadrivendiscovery/d3m.git@devel

``--process-dependency-links`` argument is required for correct
processing of dependencies.

When cloning a repository, clone it recursively to get also git
submodules:

::

    $ git clone --recursive https://gitlab.com/datadrivendiscovery/d3m.git
